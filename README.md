# fablabescape

FabLab Escape is an Escape Game funded by Grenoble University in 2018 to help people discover how a FabLab work in a fun way. You will find all the documentation you'll need [here, on the wiki home page](https://gitlab.com/FabLabEscape/fablabescape/-/wikis/home). All files to build and animate the game are in this repository.
We hope you'll enjoy building and playing this game.

Please feel free to ask questions and share your news with us at contact@fablabescape.fr or on Twitter with [Charlie Carpene](https://twitter.com/charliecarpene) and [Prismatik](https://twitter.com/PrismatikFrance). As well, please respect the [CC-BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/) non-commercial / share-alike licence we choose to use for this game. If you wish to use it in some other ways, contact us.
All the best <3
