|        Type       |   Nom                                             | Qté    | Lien |
| :-----------:     | :------                                           | ---:   |:----:|
|        :          | Contreplaqué de peuplié (400x600x5mm)             | x19    |[regarder](https://www.leroymerlin.fr/v3/p/produits/predecoupe-contreplaque-peuplier-ep-5-mm-l-120-x-l-60-cm-e154662)
|   **Plaques**     | PMMA Acrylique (400x600x5mm)                      | x1     |[regarder](https://www.strativer.fr/nos_Produits/Lextrieur/)
|        :          | PMMA Acrylique (400x600x3mm)                      | x1     |
|-------------------|---------------------------------------------------|--------|------|
|        :          | Arduino Uno                                       | x2     |[regarder](https://www.aliexpress.com/item/32665372585.html?spm=2114.12010615.8148356.2.13511ee6ljPYTb) |
|        :          | Touch Pad                                         | x1     |[regarder](https://www.aliexpress.com/item/32602940759.html?spm=2114.12010612.8148356.59.28fc63c6Z9uo3b)       |
|        :          | Timer                                             | x1     |[regarder](https://www.aliexpress.com/item/32627662931.html?spm=2114.12010615.8148356.13.65044da3oUeYAM)
|        :          | LED verte                                         | x4     |
|        :          | LED jaune                                         | x1     |
|        :          | Servo moteur SG90                                 | x2     |[regarder](https://www.aliexpress.com/item/32864422029.html?spm=a2g0o.productlist.0.0.57a026b1isyJCT&algo_pvid=6d91f19a-ee5b-476b-85e4-37e3a9119e73&algo_expid=6d91f19a-ee5b-476b-85e4-37e3a9119e73-1&btsid=9dec644e-ca49-4c13-8b94-68a38dce655d&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Capteur piezo                                     | x1     |[regarder](https://www.aliexpress.com/item/32655784125.html?spm=a2g0o.productlist.0.0.5c3f73792eaugm&algo_pvid=35b10365-f168-4458-bd53-3ff9e6d15d83&algo_expid=35b10365-f168-4458-bd53-3ff9e6d15d83-4&btsid=4f2f7ba9-42c7-4405-8d72-892af5faf900&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Résistance 220O                                   | x2     |
|        :          | Résistance 1Mo                                    | x1     |
| **Electronique**  | Carte à trou 2.54                                 |        |[regarder](https://www.aliexpress.com/item/32858951350.html?spm=a2g0o.productlist.0.0.1cd31ad7lE8ZV4&algo_pvid=8b260156-255a-4495-9716-9383b53841d3&algo_expid=8b260156-255a-4495-9716-9383b53841d3-2&btsid=6beb28bd-cb5b-442a-b689-5be603dfc83c&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Conecteur mâle 2.54                               |        |[regarder](https://www.aliexpress.com/item/33006757391.html?spm=a2g0o.productlist.0.0.4511543biFYvfX&algo_pvid=b20a08d5-c9a2-4fce-b375-cb9cddaa9d51&algo_expid=b20a08d5-c9a2-4fce-b375-cb9cddaa9d51-2&btsid=1620eee3-42d4-4856-b301-2771d2e3c77e&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Câblage & matériel à souder                       |        |
|        :          | Lampe 230V                                        | x1     |
|        :          | Clé USB                                           | x2     |
|        :          | Ralonge USB >30cm                                 | x1     |[regarder](https://www.aliexpress.com/item/32415077335.html?spm=a2g0o.productlist.0.0.2d9d7995XtxGSN&algo_pvid=546c75fc-2bc5-41f4-82ec-4d88b2eaa1ad&algo_expid=546c75fc-2bc5-41f4-82ec-4d88b2eaa1ad-0&btsid=0bb4a3a3-10cb-4756-8154-ffd80b38b8d5&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Multiprise (3 prises)                             | x1     |[regarder](https://www.aliexpress.com/item/32992100151.html?spm=a2g0o.productlist.0.0.189128b5fQ27uU&algo_pvid=e5c8056c-d5db-4e0b-9e91-86d9e8cf8ee3&algo_expid=e5c8056c-d5db-4e0b-9e91-86d9e8cf8ee3-7&btsid=dd7823e8-a931-4c3c-9fc6-ce6ffe2fb1c9&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Câble d'alimentation Arduino                      | x2     |
|        :          | Chargeur Arduino                                  | x2     |
|        :          | Ordinateur                                        | x1     |
|-------------------|---------------------------------------------------|--------|------|
|         :         | Boulon (4mm)                                      | x14    |
| **Quincaillerie** | Boulon tête plate (6mm)                           | x1     |
|         :         | Ecrou                                             | x10    |
|         :         | Insert (M4)                                       | x7     |[regarder](https://www.leroymerlin.fr/v3/p/produits/lot-de-8-manchons-a-visser-acier-brut-hettich-l-8-mm-e22181)
|-------------------|---------------------------------------------------|--------|------|
|         :         | Colle à bois                                      | x1     |
| **Adhésif**       | Colle époxy                                       | x1     |
|         :         | Scotch noir (type électrique)                     | x1     |
|-------------------|---------------------------------------------------|--------|------|
|         :         | Papier blanc A4                                   | x4     |
|         :         | Transparent imprimable A4                         | x1     |
| **Autre**         | Fiole (H: 145mm ; D: 25mm) + bouchon              | x4     |
|         :         | Colorant alimentaire (bleu, rouge, jaune, vert)   | x4     |
|         :         | Vinyle bleu                                       |        |
|         :         | Patin                                             | x3     |