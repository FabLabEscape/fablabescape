#include <TM1637Display.h>
#include <Keypad.h>

// Made by : Lanoy Nello
// lanoynello@nelowgames.art
// all serial are for debugging purpose,
// pin 1-2 can be use but you may loose those debugs

// clignoter pour les 5 dernières minutes

//////////////////////////////////////////////////////////////////////// #region Declarations



// To count array Length
#define ArrayCount(array) (sizeof array / sizeof array[0])


// CONDITIONS
bool conditionCrocodiles = false;
bool conditionKeyPadEntered = false;


// PINS
const int led_builtin = 13;
const int led_crocoOne = A0;
const int led_crocoTwo = A1;
const int led_keypadCodeEntered = A2;
const int led_keypadPressed = A3;
const int led_allConditionsTrue = A4;

const int crocoOnePin = 11;
const int crocoTwoPin = 12;

const int digit_DIO = 10;
const int digit_CLK = 9;


// TIMER
const bool resetingTimerLaunchIt = false;
const bool resetingStopsIt = false;

unsigned long timer=millis ();
bool isPaused = false;
int lastMillis = 0; // do not touch, it's for pause calculations
int timeToWait = 02; // defaut time in mins, can be changed later (Keypad)
const int timeToBlink = 05; // when does the timer starts blinking ?
TM1637Display display(digit_CLK, digit_DIO);

const int lightKeyTime = 100; // millis to LightUp the LED when Keypad is Pressed
int debugLightMillis;

//KEYPAD

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
byte rowPins[ROWS] = {5, 4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {8, 7, 6}; //connect to the column pinouts of the keypad

char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//KEYPAD CODES
const int codeMaxLength = 6; // code Length + 2 ( space to insert data after code (like mins for the set timer method) )

////// CODES LINES EXPLANATIONS :
// HOW TO USE :
// 1 -> if you want 5 digits codes, then set codeMaxLength to 5 + 2 (7)
// 2 -> set the different codes below to match up their length with the codeMaxLength
// 3 -> organize them corespondly to :
// First  line  -> Counting from zero to the same amount of time;
// Second line  -> Keep the counted time but up to the new amount;
// Third  line  -> Counting from zero to the new amount of time;
// Fourth line  -> Pause timer;
// Fith line  -> Player's code;

// 4 (build) -> codes 1-3 take argument, on the keypad, enter the code AND the argument (1234, 23 -> code 1234 to 12 mins)

char codes [5] [codeMaxLength - 2] = {
  {'0', '0', '0', '0'}, // Counting from zero to the same amount of time
  {'1', '1', '1', '1'}, // Keep the counted time but up to the new amount
  {'2', '2', '2', '2'}, //Counting from zero to the new amount of time and erase the player's code
  {'3', '3', '3', '3'}, //The Code to pause timer
  {'3', '9', '4', '6'} //The Code ToValidate by the player
};




//////////////////////////////////////////////////////////////////////// #region Player




void CheckCrocodiles () {

  // PINCES CROCODILES
  int crocoOne = !digitalRead(crocoOnePin);
  int crocoTwo = !digitalRead(crocoTwoPin);

  conditionCrocodiles = (crocoOne && crocoTwo );


  // also chechking keys
  char key = keypad.getKey();

  analogWrite(led_crocoOne, crocoOne * 255);
  analogWrite(led_crocoTwo, crocoTwo * 255);
  analogWrite(led_keypadCodeEntered , conditionKeyPadEntered * 255);

  analogWrite(led_allConditionsTrue  , ( (conditionKeyPadEntered && (crocoOne && crocoTwo ) ) ? 255 : 0 ) );
  if ( conditionKeyPadEntered && (crocoOne && crocoTwo ) )
    isPaused = true;

  if (millis () - debugLightMillis > lightKeyTime) {
    analogWrite( led_keypadPressed, 0);
  }
}

void PlayerCodeEntered () {
  Serial.println("PlayerEntered his code");
  conditionKeyPadEntered = true;
}

void LogKeyPressed() {
  // Do Some Light Thing
  analogWrite( led_keypadPressed, 255);
  debugLightMillis = millis ();
}




//////////////////////////////////////////////////////////////////////// #region Timer




void ResetTimer () {
  if (resetingTimerLaunchIt) {
    isPaused = false;
  } else if (resetingStopsIt) {
    isPaused = true;
  }
  timer = millis();
}

int SetTime (int mins) {
  //timer = millis();
  timeToWait = mins - 1; // 1 min = 60 000 millis
}

void PlayPause () {
  isPaused = !isPaused;

  Serial.print("pause : ");
  Serial.println(isPaused);
}


/*
  int GetTime () { // return min min : s s

  //int t = timeToWait - (millis() -timer) / 1000 ;
  int mins = (millis() - timer) / 100;
  int seconds = ((millis() - timer) / 1000) % 60;

  return mins * 100 + seconds;
  } */

int GetTimeReversed () { // return min min : s s
  if (isPaused) {
    int millisStepness = millis() - lastMillis;
    timer += millisStepness;
    //Serial.print("Should temporise for : ");
    //Serial.println(millisStepness);
  }
  lastMillis = millis();

  //int t = timeToWait - (millis() -timer) / 1000 ;
  int mins = (millis() - timer) / 60000;
  int seconds = ((millis() - timer) / 1000) % 60;
  Serial.print("seconds ");
  Serial.println(seconds);
  Serial.print("mins ");
  Serial.println(mins);

  Serial.print("timeToWait ");
  Serial.println(timeToWait);

  return Clamp(timeToWait - mins, 0, 60) * 100 + ( 59 - seconds);
}

int Clamp ( int value, int min, int max ) {
  if(value < min) {
    return min;
  } else if ( value > max ){
    return max;
  } else {
    return value;
  }
}


//////////////////////////////////////////////////////////////////////// #region Common




void setup() {
  Serial.begin(9600); // to debug
  Serial.println("Starting serial");

  // initialize digital pin LED_BUILTIN as an output.
  pinMode(led_builtin, OUTPUT);

  pinMode(crocoOnePin, INPUT_PULLUP);
  pinMode(crocoTwoPin, INPUT_PULLUP);

  pinMode(led_crocoOne , OUTPUT);
  pinMode(led_crocoTwo , OUTPUT);
  pinMode(led_keypadCodeEntered, OUTPUT);
  pinMode(led_keypadPressed , OUTPUT);
  pinMode(led_allConditionsTrue , OUTPUT);

  analogWrite(led_crocoOne , 0);
  analogWrite(led_crocoTwo , 0);
  analogWrite(led_keypadCodeEntered , 0);
  analogWrite(led_keypadPressed , 0);
  analogWrite(led_allConditionsTrue , 0);


  uint8_t data[4];
  display.setBrightness(1, false); // Turn on
  display.setSegments(data);

  ResetTimer ();
  SetTime ( timeToWait );

  keypad.addEventListener(keypadEvent); // Add an event listener for this keypad

  lastMillis = millis ();
}

void loop() {
  CheckCrocodiles ();

  // DIGIT SCREEN
  uint8_t data[4];

  int _time = GetTimeReversed();
  if (_time <= 0) {
    _time = 0;
    isPaused = true;
    display.setBrightness( 0, false);
    display.setSegments(data);

  } else if (_time > 0) {

    data[3] = display.encodeDigit( _time % 10);
    _time = _time / 10;
    data[2] = display.encodeDigit( _time % 10);
    _time = _time / 10;
    data[1] = display.encodeDigit( _time % 10);
    bool doesBlinking = ( _time < timeToBlink );
    _time = _time / 10;
    data[0] = display.encodeDigit( _time % 10);


    display.setBrightness(7, true); // Turn on
    display.setSegments(data);
    delay(200);

    CheckCrocodiles ();

    int _millis = millis();
    if (!isPaused && doesBlinking ) {
      for ( int i = 0; i <= 7; i ++ ) {
        display.setBrightness( 7 - i, true);
        display.setBrightness( 7 - ( (millis() - _millis) * 350 ) % 7, true);
        display.setSegments(data);
        delay(50);
        CheckCrocodiles ();
      }

      display.setBrightness( 0, false);
      display.setSegments(data);
      delay(250);

    } else {
      display.setBrightness( 7, true);
      display.setSegments(data);
      CheckCrocodiles ();
    }

    //CheckCrocodiles ();

  }

  if ( conditionCrocodiles || conditionKeyPadEntered ) {
    digitalWrite(led_builtin, !digitalRead(led_builtin) );
    //digitalWrite(pinLed, HIGH );
  } else {
    digitalWrite(led_builtin, LOW );
  }
  if ( conditionCrocodiles && conditionKeyPadEntered ) {
    digitalWrite(led_builtin, HIGH );
  }

  //DEBUG LIGHTS

  //digitalWrite (pinLed, conditionCrocodiles );
  //digitalWrite(pinLed, HIGH );
}




//////////////////////////////////////////////////////////////////////// #region Keypad




char actualCode[codeMaxLength];// = {0, 0, 0, 0, 0, 0};
int codeIndex = 0;

void AddToCode (char key) {
  if ( codeIndex >= codeMaxLength ) {
    Serial.println("Code at max Length");
    return;
  }
  actualCode[codeIndex] = key;

  Serial.print(key);
  Serial.print(" at case : ");
  Serial.println(codeIndex);

  codeIndex ++;
}

void ValidateCode () {
  if (codeIndex < codeMaxLength - 2) {
    Serial.print(codeIndex);
    Serial.println(" is the code length");
    return;
  }

  Serial.println(actualCode);


  for (int i = 0; i < ArrayCount(codes); i ++ ) {
    bool isCodeKValid = true;
    for ( int k = 0; k < ArrayCount(codes[i]); k ++) {
      Serial.print(k);
      Serial.print(" | comparing :");
      Serial.print(codes[i][k]);
      Serial.print(" with : ");
      Serial.println(actualCode[k]);
      if (codes[i][k] != actualCode[k]) {
        Serial.println("InValid Code");
        isCodeKValid = false;
        break;
      }
    }
    if (isCodeKValid) {
      int _mins = (int) ( actualCode[codeMaxLength - 2] - '0' ) * 10 + ( actualCode[codeMaxLength - 1] - '0' );

      switch (i) {
        case 0: // {0, 0, 0, 0}
          Serial.println("Restarting Timer");
          ResetTimer (); // counting for the same mins a new time
          break;
        case 1: // {1,4,8,4}
          Serial.println("Changing Max Time");
          SetTime ( _mins ); // change time to wait but not the counter (it's adding prolongations)
          break;
        case 2: // {3, 9, 4, 7}
          ResetTimer ();
          conditionKeyPadEntered = false;
          Serial.print("Set Time for :");
          Serial.println(_mins);
          SetTime ( _mins );
          break;
        case 3 : // {'3', '9', '4', '8'}
          PlayPause ();
          break;
        case 4 : // {'3', '9', '4', '8'}
          PlayerCodeEntered ();
          break;
      }
      break;
    }
  }

  AnnulateCode ();
  Serial.println(' ');
}
void AnnulateCode () {
  //actualCode = new char {0, 0, 0, 0, 0, 0};
  codeIndex = 0;
}

void keypadEvent(KeypadEvent key) {
  //Serial.println(key);

  switch (keypad.getState()) {
    case PRESSED:
      if (key == '#') {
        Serial.println("Annulation du code ...");
        AnnulateCode ();
      } else if (key == '*') {
        Serial.println("Validation du code ...");
        ValidateCode ();
      } else {
        AddToCode (key);
      }
      LogKeyPressed();
      break;

    case RELEASED:
      if (key == '*') {
      }
      break;

    case HOLD:
      if (key == '*') {
      }
      break;
  }
}

