|        Type       |   Name                                            | Qty    |  Link  |
| :-----------:     | :------                                           | ---:   | :-----:|
|        :          | Poplar plywood (400x600x5)                        | x19    |        |
|     **Plate**     | PMMA Acrylic (400x600x5)                          | x1     |        |
|        :          | PMMA Acrylic (400x600x3)                          | x1     |        |
|-------------------|---------------------------------------------------|--------|--------|
|        :          | Arduino Uno                                       | x2     | [check](https://www.aliexpress.com/item/32665372585.html?spm=2114.12010615.8148356.2.13511ee6ljPYTb) |
|        :          | Touch Pad                                         | x1     | [check](https://www.aliexpress.com/item/32602940759.html?spm=2114.12010612.8148356.59.28fc63c6Z9uo3b)       |
|        :          | Timer                                             | x1     | [check](https://www.aliexpress.com/item/32627662931.html?spm=2114.12010615.8148356.13.65044da3oUeYAM)
|        :          | LED (green)                                       | x4     |
|        :          | LED (yellow)                                      | x1     |
|        :          | SG90 servomotor                                   | x2     | [check](https://www.aliexpress.com/item/32864422029.html?spm=a2g0o.productlist.0.0.57a026b1isyJCT&algo_pvid=6d91f19a-ee5b-476b-85e4-37e3a9119e73&algo_expid=6d91f19a-ee5b-476b-85e4-37e3a9119e73-1&btsid=9dec644e-ca49-4c13-8b94-68a38dce655d&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Piezo sensor                                      | x1     | [check](https://www.aliexpress.com/item/32655784125.html?spm=a2g0o.productlist.0.0.5c3f73792eaugm&algo_pvid=35b10365-f168-4458-bd53-3ff9e6d15d83&algo_expid=35b10365-f168-4458-bd53-3ff9e6d15d83-4&btsid=4f2f7ba9-42c7-4405-8d72-892af5faf900&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | 220O resistor                                     | x2     |
|        :          | 1MO resistor                                      | x1     |
|  **Electronic**  | PCB board 2.54                                    |        | [check](https://www.aliexpress.com/item/32858951350.html?spm=a2g0o.productlist.0.0.1cd31ad7lE8ZV4&algo_pvid=8b260156-255a-4495-9716-9383b53841d3&algo_expid=8b260156-255a-4495-9716-9383b53841d3-2&btsid=6beb28bd-cb5b-442a-b689-5be603dfc83c&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Pin 2.54mm                                        |        | [check](https://www.aliexpress.com/item/33006757391.html?spm=a2g0o.productlist.0.0.4511543biFYvfX&algo_pvid=b20a08d5-c9a2-4fce-b375-cb9cddaa9d51&algo_expid=b20a08d5-c9a2-4fce-b375-cb9cddaa9d51-2&btsid=1620eee3-42d4-4856-b301-2771d2e3c77e&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Soldering tools and cables                        |        |
|        :          | Lamp (230V)                                       | x1     |
|        :          | USB Key                                           | x2     |
|        :          | USB extension cord >30cm                          | x1     | [check](https://www.aliexpress.com/item/32415077335.html?spm=a2g0o.productlist.0.0.2d9d7995XtxGSN&algo_pvid=546c75fc-2bc5-41f4-82ec-4d88b2eaa1ad&algo_expid=546c75fc-2bc5-41f4-82ec-4d88b2eaa1ad-0&btsid=0bb4a3a3-10cb-4756-8154-ffd80b38b8d5&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Multi-socket  (3 holes)                           | x1     | [check](https://www.aliexpress.com/item/32992100151.html?spm=a2g0o.productlist.0.0.189128b5fQ27uU&algo_pvid=e5c8056c-d5db-4e0b-9e91-86d9e8cf8ee3&algo_expid=e5c8056c-d5db-4e0b-9e91-86d9e8cf8ee3-7&btsid=dd7823e8-a931-4c3c-9fc6-ce6ffe2fb1c9&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|        :          | Arduino power cable                               | x2     |
|        :          | Arduino USB cable                                 | x2     |
|        :          | Computer                                          | x1     |
|-------------------|---------------------------------------------------|--------|--------|
|         :         | Bolt (4mm)                                        | x14    |
|    **Hardware**   | Flat head blot (6mm)                              | x1     |
|         :         | Nut                                               | x10    |
|         :         | Insert (M4)                                       | x7     | [check](https://www.aliexpress.com/item/32756497854.html?spm=a2g0o.productlist.0.0.71015840nF5muZ&algo_pvid=83da12ba-7826-414f-b52d-780d656fe501&algo_expid=83da12ba-7826-414f-b52d-780d656fe501-0&btsid=a8125b39-0c3c-4e80-9f3c-c3cfaa6f61cc&ws_ab_test=searchweb0_0,searchweb201602_10,searchweb201603_55)
|-------------------|---------------------------------------------------|--------|--------|
|         :         | Wood glue                                         | x1     |
|    **Adhesive**   | Epoxy glue                                        | x1     |
|         :         | Black tape                                        | x1     |
|-------------------|---------------------------------------------------|--------|--------|
|         :         | A4 white paper                                    | x4     |
|         :         | A4 transparent paper                              | x1     |
|     **Other**     | flask (H: 145mm ; D: 25mm) + cap                  | x4     |
|         :         | Food colouring (blue, red, yellow, green)         | x4     |
|         :         | Blue vinyl                                        |        |
|         :         | Slider                                            | x3     |