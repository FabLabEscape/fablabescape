MEDIBOT TK-4500 : SCANNER MODULE

RIGHT CLIC ON THE SCANs.zip ARCHIVED FILE -> Extract here -> PASSWORD: README

INSTRUCTIONS:


Once a patient have been scanned, MEDIBOT TK-4500 automatiquely generate 5 files.

Each of these files shows majors injuries from the patient and need to be processed carefully.

Those files are located in the SCANs.zip file which you can access with a right clic and "Extract here".

To handle better all scans, print it if it is not corrupted.


F.A.Q (Frequently Asked Questions)

1. One of my scans seems to be cutter in pieces.
2. Four of my scans look likes the same. 
3. Why can't I go on?
4. The audio file is corrupted
5. One scan cannot open at all.
6. The computer is slow.
7. An archive file is generated but secured with a password
8. There is a cat inside the box.


---


1. One of my scans seems to be cutter in pieces

This problem occurs when a patient have had previous surgery problems.

Such file should have the extension .xcf.

Open it with "GIMP" software.

Make sure you are able to see all layers ("eye" symbol in the Layer section, on the right).

If not, clic on the checkbox where the symbol "eye" should be to make the layer appear.

Use "move" tool (shortcut "m") to join correctly all layers together.


2. Four of my scans look likes the same. 

There is a probability of 0.1% that the scanner returns an unclear message due to an uncertainty of measurement.

In that case as many scans as uncertainty are returned to show all possible interpretations.

An audio file will be generated as well to help select the most likely scan. 


3. Why can't I continue?

Can't you? Maybe keep readying this README.txt ;)


4. The audio file is corrupted

An extremely rare anomaly can happen when the audio file is generated (see Question 2).
This file needs to be processed with "Audacity"'s software.

Open the software and drap & drop the audio file in the main window.

Several processes are possible depending on the anomaly



- Amplify the signal: "Effect" -> "Amplify", then choose the amplify level (in decibels, sound level unit)


- Reverse the signal: "Effect" -> "Reverse"


- Cut off the signal: select the part you want to cut then press "Delete" on your keyboard.



5. One scan cannot open at all.



Check the extensions of the files and change it if needed.

If extensions are not visible from the File Explorer, go to menu "View" and check "File name extensions"


6. The computer is slow.

Sorry, nothing we can do about that. Hopefully you'll managed it anyway: computer should not be stronger than you.


7. An archive file is generated but secured with a password



[Find the password]
A secret .SVG encoding system file has been set up to fin the password.

Open the file with "Inkscape"'s software.
Zoom if needed (shortcut F3).

Link the BLACK dots with "Draw Bezier curves and straight lines"'s tool (shortcut MAJ + F6)


8. There is a cat inside the box

Maybe there is, maybe not.
Depending on your score we'll tell you so hurry up instead of reading this: you're done with the README.txt now !